WF Torch Code Evaluation

Purpose:
Candidate will demonstrate proficiency in constructing a basic custom post type (WordPress custom content type) and a Gutenberg Block (front-end component) to output the CMS data from the post type.

Tools Needed:
Bitbucket/Sourcetree: download code repository and database
MAMP or other local LAMP runtime
Adobe XD (free trial available): source file
Gulp: SASS and JS taskrunner
Code Editor of your choice
ACF WP Plugin: Provided in repo
Joints WP Theme: Provided in repo

Objective:
Create a new branch of code in the repo for your work
Create a custom post type ‘Products’ using the fields that are apparent from the source file
Create a custom Gutenberg block to output the ‘Products’ post type information on the home page of the website
Export your database and commit your changes to your branch

Documentation/Resources:
Theme: http://jointswp.com/docs/
ACF: https://www.advancedcustomfields.com/blog/acf-5-8-introducing-acf-blocks-for-gutenberg/


Login Information:

Database

user: wildfire
pass: wildfire


WordPress:
/wp-admin/
user: wildfire
pass: wildfire